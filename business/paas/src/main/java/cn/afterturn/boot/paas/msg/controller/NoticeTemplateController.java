        /**
 * Copyright 2017-2019 JueYue (qrb.jueyue@foxmail.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.afterturn.boot.paas.msg.controller;

import cn.afterturn.boot.bussiness.base.controller.BaseController;
import cn.afterturn.boot.paas.msg.model.NoticeTemplateModel;
import cn.afterturn.boot.paas.msg.service.INoticeTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 通知模板控制器
 *
 * @author JueYue
 * @Date 2019-08-16 17:09:52
 */
@Api("通知模板")
@RestController
@RequestMapping("/notice/template")
public class NoticeTemplateController extends BaseController<INoticeTemplateService, NoticeTemplateModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(NoticeTemplateController.class);

    @Autowired
    private INoticeTemplateService noticeTemplateService;

}