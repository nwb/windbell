package cn.afterturn.boot.paas.im.thirdservice.weixin.clients;

import cn.afterturn.boot.paas.PaasApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by dbinary on 2019/7/22
 * <p>
 * /
 * /**
 *群聊發送信息
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PaasApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WeiXinWeChatClientTest {
    /**
     *
     */
    @Test
    public void create() {
    }

    @Test
    public void update() {
    }

    @Test
    public void get() {
    }

    @Test
    public void sendText() {
    }

    @Test
    public void sendPicture() {
    }

    @Test
    public void sendVoice() {
    }

    @Test
    public void sendFile() {
    }

    @Test
    public void sendVeido() {
    }

    @Test
    public void sendTextcard() {
    }

    @Test
    public void sendNews() {
    }

    @Test
    public void sendMpNews() {
    }

    @Test
    public void sendMarkdown() {
    }
}
